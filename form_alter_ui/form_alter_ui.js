
Drupal.behaviors.formAlterUi = function (context) {
  $('form:not(.form-alter-ui-processed)').not('#form-alter-ui-element').each(function () {
    var formId = $('input[name=form_id]', this).attr('value');
    var url = $(this).attr('action');
    url += ((url.indexOf('?') == -1) ? '?' : '&') +'form_alter_ui_form_id=' + formId;
    $(this)
      .addClass('form-alter-ui-processed')
      .prepend('<div class="form-alter-ui-enable"><label><input class="form-alter-ui-enable" type="checkbox" /> '+ Drupal.t('Form customization mode') +'</label></div>')
      .find('input.form-alter-ui-enable')
      .click(function () {
        var checked = $(this).is(':checked');
        $('.element-marker-marked', this.form)
          .each(function () {
            var id = $(this).attr('id');
            var marker = Drupal.settings.element_marker[id];
            // Only process if we have data available.
            if (marker) {
              var a = document.createElement('a');
              $(a)
                .attr('href', url +'&form_alter_ui_element='+ marker.join('|'))
                .addClass('form-alter-ui');
              if (checked) {
                var link = $('#'+ id +'-wrapper')
                  .wrap(a)
                  .mouseover(function() {
                    $(this).addClass('form-alter-ui-active');
                  })
                  .mouseout(function() {
                    $(this).removeClass('form-alter-ui-active');
                  })
                  .parent('a.form-alter-ui')
                  .get(0);
                Drupal.behaviors.popups(link);
              }
              else {
                $('#'+ id +'-wrapper')
                  .unbind('mouseover')
                  .unbind('mouseout')
                  .parent('a')
                  .replaceWith($('#'+ id +'-wrapper').get(0));
              }
            }
          });
      });
  });
};
